package ui.registration;

import com.google.gson.JsonObject;
import driver.DriverManager;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.exception.JsonPathException;
import io.restassured.response.Response;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import page.DeleteAccountPage;
import page.LoginPage;
import page.MainPage;
import page.RegistrationPage;
import ui.BaseUITest;
import utils.TestDataReader;
import io.restassured.*;

import static io.restassured.RestAssured.*;

public class SuccessfulRegistration extends BaseUITest {
    private static final Logger logger= LoggerFactory.getLogger(SuccessfulRegistration.class);
    private MainPage mainPage;
    private RegistrationPage registrationPage;
    private LoginPage loginPage;
    private DeleteAccountPage deleteAccountPage;
    private final String fullNameDataSource ="registration.successfulRegistration.fullName";
    private final String emailDataSource  ="registration.successfulRegistration.email";
    private final String passwordDataSource="registration.successfulRegistration.password";
    private String fullName;
    private String email;
    private String password;

    @BeforeMethod
    public void setup()  {
        logger.info("Reading test Data from file");
        fullName= TestDataReader.getTestData(fullNameDataSource);
        email=TestDataReader.getTestData(emailDataSource);
        password=TestDataReader.getTestData(passwordDataSource);
        logger.info("Check test data for already register user");
        RestAssured.baseURI="https://stepik.org";
        RestAssured.basePath="/api/users/login";
        Response getCookies=given()
                .header("Referer","https://stepik.org/catalog")
                .get(baseURI+"/catalog?auth=login")
                .then()
                .extract()
                .response();
        requestSpecification=given()
                .filter(new AllureRestAssured())
                .contentType(ContentType.JSON)
                .header("Referer","https://stepik.org/catalog?auth=login")
                .header("X-CSRFToken", getCookies.cookie("csrftoken"))
                .header("Accept-Language","ru-RU,ru;q=0.9")
                .cookie("sessionid",getCookies.cookie("sessionid"))
                .cookie("csrftoken",getCookies.cookie("csrftoken"));
        JsonObject requestParams=new JsonObject();
        requestParams.addProperty("email",email);
        requestParams.addProperty("password",password);
        String requestBody=requestParams.toString();
        Response loginResponse=given()
                .spec(requestSpecification)
                .body(requestBody)
                .post()
                .then()
                .extract()
                .response();
        String detail= "";
        try {
            detail = loginResponse.jsonPath().get("detail");
        }
        catch (JsonPathException e) {
            logger.error("User is already registered. User will be deleted for test!");
            deleteUser();
        }
        if (detail.equals("The e-mail address and/or password you specified are not correct."))
        {
            logger.info("OK. Test user does not exist.");
        }
    }
    @Test
    @Severity(SeverityLevel.BLOCKER)
    public void successfulRegistrationTest() {
        MainPage mainPage=new RegistrationPage(driver).
                openPage().
                inputName(fullName).
                inputEmail(email).
                inputPassword(password).
                registerUser();
        Assert.assertTrue(mainPage.isUserLogged());
    }
    @AfterTest(description = "Удаление созданного аккаунта")
    public void close() {
        driver.quit();
        deleteUser();
    }
    public void deleteUser()    {
        logger.info("Deleting test account");
        WebDriver driver1=DriverManager.getDriver();
        deleteAccountPage=new LoginPage(driver1)
                .openPage()
                .inputLogin(email)
                .inputPassword(password)
                .logUserToDeleteUser()
                .openPage()
                .deleteAccount(email,password);
        driver1.close();
    }
}
