package ui.registration;

import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import page.RegistrationPage;
import ui.BaseUITest;
import utils.TestDataReader;

public class ExistedAccount extends BaseUITest {
    private static final Logger logger= LoggerFactory.getLogger(ExistedAccount.class);
    private RegistrationPage registrationPage;
    private final String fullNameDataSource ="registration.existedAccount.fullName";
    private final String emailDataSource  ="registration.existedAccount.email";
    private final String passwordDataSource="registration.existedAccount.password";
    private String fullName;
    private String email;
    private String password;

    @BeforeMethod
    public void setup()  {
        logger.info("Reading test Data from file");
        fullName= TestDataReader.getTestData(fullNameDataSource);
        email=TestDataReader.getTestData(emailDataSource);
        password=TestDataReader.getTestData(passwordDataSource);
    }

    @Test
    @Severity(SeverityLevel.BLOCKER)
    public void existedAccountTest() {
        registrationPage=new RegistrationPage(driver).
                openPage().
                inputName(fullName).
                inputEmail(email).
                inputPassword(password).
                tryRegisterUser();
        Assert.assertTrue(registrationPage.isVisibleExistedAccountAlert());
    }
}
