package ui.registration;

import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.PrivacyPage;
import page.RegistrationPage;
import ui.BaseUITest;

public class PrivacyLink extends BaseUITest {
    private static final Logger logger= LoggerFactory.getLogger(PrivacyLink.class);
    private RegistrationPage registrationPage;
    private PrivacyPage privacyPage;

    @Test
    @Severity(SeverityLevel.CRITICAL)
    public void PrivacyLinkTest()   {
        privacyPage=new RegistrationPage(driver)
                .openPage()
                .clickPrivacy()
                .switchTabToPrivacyPage();
        Assert.assertTrue(privacyPage.isCurrentPagePrivacy());
    }

}
