package ui.registration;

import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import page.RegistrationPage;
import ui.BaseUITest;
import utils.TestDataReader;

public class EmptyInput extends BaseUITest {
    private static final Logger logger= LoggerFactory.getLogger(EmptyInput.class);
    private RegistrationPage registrationPage;
    private final String fullNameDataSource ="registration.emptyInput.fullName";
    private final String emailDataSource  ="registration.emptyInput.email";
    private final String passwordDataSource="registration.emptyInput.password";
    private String fullName;
    private String email;
    private String password;

    @BeforeMethod
    public void setup()  {
        logger.info("Reading test Data from file");
        fullName= TestDataReader.getTestData(fullNameDataSource);
        email=TestDataReader.getTestData(emailDataSource);
        password=TestDataReader.getTestData(passwordDataSource);
    }

    @Test
    @Severity(SeverityLevel.BLOCKER)
    public void emptyInputTest() {
        registrationPage=new RegistrationPage(driver).
                openPage().
                inputName(fullName).
                inputEmail(email).
                inputPassword(password).
                tryRegisterUser();
        Assert.assertTrue(registrationPage.isRegistrationFormVisible());
    }
}
