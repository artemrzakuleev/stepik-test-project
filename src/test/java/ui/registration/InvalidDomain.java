package ui.registration;

import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import page.RegistrationPage;
import ui.BaseUITest;
import utils.TestDataReader;

public class InvalidDomain extends BaseUITest {
    private static final Logger logger= LoggerFactory.getLogger(InvalidDomain.class);
    private RegistrationPage registrationPage;
    private final String fullNameDataSource ="registration.invalidDomain.fullName";
    private final String emailDataSource  ="registration.invalidDomain.email";
    private final String passwordDataSource="registration.invalidDomain.password";
    private String fullName;
    private String email;
    private String password;

    @BeforeMethod
    public void setup()  {
        logger.info("Reading test data from file");
        fullName= TestDataReader.getTestData(fullNameDataSource);
        email=TestDataReader.getTestData(emailDataSource);
        password=TestDataReader.getTestData(passwordDataSource);
    }

    @Test
    @Severity(SeverityLevel.BLOCKER)
    public void invalidDomainTest(){
        registrationPage=new RegistrationPage(driver).
                openPage().
                inputName(fullName).
                inputEmail(email).
                inputPassword(password).
                tryRegisterUser();
        Assert.assertTrue(registrationPage.isVisibleInvalidEmailAddressAlert());
    }
}
