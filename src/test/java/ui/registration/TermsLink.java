package ui.registration;

import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.RegistrationPage;
import page.TermsPage;
import ui.BaseUITest;

public class TermsLink extends BaseUITest {
    private static final Logger logger= LoggerFactory.getLogger(TermsLink.class);
    private RegistrationPage registrationPage;

    @Test
    @Severity(SeverityLevel.CRITICAL)
    public void TermsLinkTest() {
        TermsPage termsPage=new RegistrationPage(driver)
                .openPage()
                .clickTerms()
                .switchTabToTermsPage();
        Assert.assertTrue(termsPage.isCurrentPageTerms());
    }
}
