package ui.sideBar;

import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import page.SideBarPage;
import ui.BaseUITest;

public class PinSideBar extends BaseUITest {
    private static final Logger logger= LoggerFactory.getLogger(PinSideBar.class);
    private SideBarPage sideBarPage;

    /**
     * Подготовительные шаги - открепление и сворачивание боковой панели
     */
    @BeforeMethod
    public void setup()  {
        logger.info("Unpinning side bar for test pre-conditions");
        sideBarPage =new SideBarPage(driver).
                openPage().
                unpinSideBar().
                changeFocus().
                waitUntilSideBarHide();
    }

    @Test
    @Severity(SeverityLevel.MINOR)
    public void pinSideBarTest() {
        sideBarPage= sideBarPage.expandSideBar()
                .pinSideBar();
        Assert.assertTrue(sideBarPage.isSideBarVisible());
    }
}
