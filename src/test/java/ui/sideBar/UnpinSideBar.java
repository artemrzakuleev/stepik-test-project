package ui.sideBar;

import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.SideBarPage;
import ui.BaseUITest;

public class UnpinSideBar extends BaseUITest {
    private static final Logger logger= LoggerFactory.getLogger(UnpinSideBar.class);
    private SideBarPage sideBarPage;

    @Test
    @Severity(SeverityLevel.MINOR)
    public void unpinSideBarTest() {
        sideBarPage =new SideBarPage(driver).
                openPage().
                unpinSideBar().
                changeFocus().
                waitUntilSideBarHide();
        Assert.assertTrue(sideBarPage.isSideBarInvisible());
    }
}
