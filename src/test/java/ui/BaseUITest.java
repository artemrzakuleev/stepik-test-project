package ui;

import driver.DriverManager;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import utils.TestExecutionListener;

@Listeners(TestExecutionListener.class)
public abstract class BaseUITest {
    private static final Logger logger = LoggerFactory.getLogger(BaseUITest.class);
    protected WebDriver driver;

    /**
     * Инициализация Selenium WebDriver для UI теста
     */
    @BeforeTest
    public void setupDriver()  {
        driver= DriverManager.getDriver();
    }

    public WebDriver getDriver()    {
        return this.driver;
    }

    @AfterTest
    public void closeDriver()   {
        driver.quit();
        logger.info("Closing webDriver");
    }
}
