package ui.languageChange;

import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.testng.Assert;
import org.testng.annotations.*;
import page.LanguagePage;
import model.Language;
import ui.BaseUITest;

public class LanguageChangeToEnglish extends BaseUITest {
    private LanguagePage languagePage;

    @Test
    @Severity(SeverityLevel.CRITICAL)
    public void changeLanguageToEnglishTest()   {
        languagePage=new LanguagePage(driver).
                openPage().
                changeLanguage(Language.ENGLISH.getTextOfLanguageButton());
        Assert.assertTrue(languagePage.checkEqualTextInSearchButtonToExpected(Language.ENGLISH.getTextOfSearchButton()));
    }
}
