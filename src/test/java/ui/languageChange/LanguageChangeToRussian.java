package ui.languageChange;

import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import model.Language;
import org.testng.Assert;
import org.testng.annotations.*;
import page.LanguagePage;
import ui.BaseUITest;

public class LanguageChangeToRussian extends BaseUITest {
    private LanguagePage languagePage;
    @Test
    @Severity(SeverityLevel.CRITICAL)
    public void changeLanguageToRussianTest()   {
        languagePage=new LanguagePage(driver).
                openPage().
                changeLanguage(Language.RUSSIAN.getTextOfLanguageButton());
        Assert.assertTrue(languagePage.checkEqualTextInSearchButtonToExpected(Language.RUSSIAN.getTextOfSearchButton()));
    }
}
