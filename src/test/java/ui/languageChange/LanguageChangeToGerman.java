package ui.languageChange;

import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import model.Language;
import org.testng.Assert;
import org.testng.annotations.*;
import page.LanguagePage;
import ui.BaseUITest;
public class LanguageChangeToGerman extends BaseUITest {
    private LanguagePage languagePage;

    @Test
    @Severity(SeverityLevel.CRITICAL)
    public void changeLanguageToGermanTest()   {
        languagePage=new LanguagePage(driver).
                openPage().
                changeLanguage(Language.GERMAN.getTextOfLanguageButton());
        Assert.assertTrue(languagePage.checkEqualTextInSearchButtonToExpected(Language.GERMAN.getTextOfSearchButton()));
    }
}
