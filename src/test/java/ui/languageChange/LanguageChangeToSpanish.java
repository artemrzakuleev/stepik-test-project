package ui.languageChange;

import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import model.Language;
import org.testng.Assert;
import org.testng.annotations.*;
import page.LanguagePage;
import ui.BaseUITest;

public class LanguageChangeToSpanish extends BaseUITest {
    private LanguagePage languagePage;

    @Test
    @Severity(SeverityLevel.CRITICAL)
    public void changeLanguageToSpanishTest()   {
        languagePage=new LanguagePage(driver).
                openPage().
                changeLanguage(Language.SPANISH.getTextOfLanguageButton());
        Assert.assertTrue(languagePage.checkEqualTextInSearchButtonToExpected(Language.SPANISH.getTextOfSearchButton()));
    }
}
