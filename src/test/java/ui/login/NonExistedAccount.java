package ui.login;

import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import page.LoginPage;
import org.testng.Assert;
import ui.BaseUITest;
import utils.TestDataReader;

public class NonExistedAccount extends BaseUITest {
    private static final Logger logger= LoggerFactory.getLogger(NonExistedAccount.class);
    private LoginPage loginPage;
    private final String emailDataSource="login.nonExistedAccount.email";
    private final String passwordDataSource="login.nonExistedAccount.password";
    private String email;
    private String password;

    @BeforeMethod
    public void setup()  {
        logger.info("Reading test data from file");
        email= TestDataReader.getTestData(emailDataSource);
        password=TestDataReader.getTestData(passwordDataSource);
    }

    @Test
    @Severity(SeverityLevel.BLOCKER)
    public void nonExistedAccountTest() {
        loginPage=new LoginPage(driver).
                openPage().
                inputLogin(email).
                inputPassword(password).
                tryLogUser();
        Assert.assertTrue(loginPage.isVisibleInvalidLoginAlert());
    }
}
