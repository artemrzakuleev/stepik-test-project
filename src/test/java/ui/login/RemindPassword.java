package ui.login;

import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.*;
import page.LoginPage;
import org.testng.Assert;
import ui.BaseUITest;

public class RemindPassword extends BaseUITest {
    private static final Logger logger= LoggerFactory.getLogger(RemindPassword.class);
    private LoginPage loginPage;

    @Test
    @Severity(SeverityLevel.BLOCKER)
    public void remindPasswordTest()    {
        loginPage=new LoginPage(driver).
                openPage().
                clickRemindPassword();
        Assert.assertTrue(loginPage.isResetFormVisible());
    }
}
