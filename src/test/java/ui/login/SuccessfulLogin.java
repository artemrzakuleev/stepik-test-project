package ui.login;

import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeMethod;
import page.LoginPage;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.MainPage;
import ui.BaseUITest;
import utils.TestDataReader;

public class SuccessfulLogin extends BaseUITest {
    private static final Logger logger= LoggerFactory.getLogger(SuccessfulLogin.class);
    private LoginPage loginPage;
    private MainPage mainPage;
    private final String emailDataSource="login.successfulLogin.email";
    private final String passwordDataSource="login.successfulLogin.password";
    private String email;
    private String password;

    @BeforeMethod
    public void setup()  {
        logger.info("Reading test data from file");
        email= TestDataReader.getTestData(emailDataSource);
        password=TestDataReader.getTestData(passwordDataSource);
    }

    @Test
    @Severity(SeverityLevel.BLOCKER)
    public void successfulLoginTest() {
        MainPage mainPage=new LoginPage(driver).
                openPage().
                inputLogin(email).
                inputPassword(password).
                logUser();
        Assert.assertTrue(mainPage.isUserLogged());
    }
}
