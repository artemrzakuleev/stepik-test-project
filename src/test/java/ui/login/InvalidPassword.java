package ui.login;

import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.*;
import page.LoginPage;
import org.testng.Assert;
import ui.BaseUITest;
import utils.TestDataReader;

public class InvalidPassword extends BaseUITest {
    private static final Logger logger= LoggerFactory.getLogger(InvalidPassword.class);
    private LoginPage loginPage;
    private final String emailDataSource="login.invalidPassword.email";
    private final String passwordDataSource="login.invalidPassword.password";
    private String email;
    private String password;

    @BeforeMethod
    public void setup()  {
        logger.info("Reading test data from file");
        email= TestDataReader.getTestData(emailDataSource);
        password=TestDataReader.getTestData(passwordDataSource);
    }

    @Test
    @Severity(SeverityLevel.BLOCKER)
    public void invalidPasswordTest() {
        loginPage=new LoginPage(driver).
                openPage().
                inputLogin(email).
                inputPassword(password).
                tryLogUser();
        Assert.assertTrue(loginPage.isVisibleInvalidLoginAlert());
    }
}
