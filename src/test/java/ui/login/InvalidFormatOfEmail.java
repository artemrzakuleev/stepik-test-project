package ui.login;

import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import page.LoginPage;
import ui.BaseUITest;
import utils.TestDataReader;

public class InvalidFormatOfEmail extends BaseUITest {
    private static final Logger logger= LoggerFactory.getLogger(NonExistedAccount.class);
    private LoginPage loginPage;
    private final String emailDataSource="login.invalidFormatOfEmail.email";
    private final String passwordDataSource="login.invalidFormatOfEmail.password";
    private String email;
    private String password;

    @BeforeMethod
    public void setup()  {
        logger.info("Reading test data from file");
        email= TestDataReader.getTestData(emailDataSource);
        password=TestDataReader.getTestData(passwordDataSource);
    }

    /**
     * Тест логина с измененным типом поля ввода емейла на текст. Проверяется серверная валидация поля ввода емейла
     */
    @Test
    @Severity(SeverityLevel.BLOCKER)
    public void invalidFormatOfEmailTest() {
        loginPage=new LoginPage(driver).
                openPage().
                changeTypeOfEmailField().
                inputLogin(email).
                inputPassword(password).
                tryLogUser();
        Assert.assertTrue(loginPage.isVisibleInvalidLoginAlert());
    }
}
