package ui.search;

import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.SearchPage;
import ui.BaseUITest;

public class Search extends BaseUITest {
    private static final Logger logger= LoggerFactory.getLogger(Search.class);
    private SearchPage searchPage;
    private final String  textForSearch="Java";

    @Test
    @Severity(SeverityLevel.CRITICAL)
    public void searchTest()    {
        searchPage=new SearchPage(driver)
                .openPage()
                .enterTextInSearchInput(textForSearch);
        Assert.assertTrue(searchPage.isSearchResultsContainsText(textForSearch));
    }
}
