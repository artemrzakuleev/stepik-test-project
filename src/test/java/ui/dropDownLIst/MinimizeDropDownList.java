package ui.dropDownLIst;

import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import page.DropDownListPage;
import ui.BaseUITest;

public class MinimizeDropDownList extends BaseUITest {
    private static final Logger logger= LoggerFactory.getLogger(MinimizeDropDownList.class);
    private DropDownListPage dropDownListPage;
    @Test
    @Severity(SeverityLevel.MINOR)
    public void minimizeDropDownList(){
        logger.info("Testing minimize of drop-down list");
        SoftAssert softAssert=new SoftAssert();
        dropDownListPage=new DropDownListPage(driver)
                .openPage()
                .moveToList()
                .minimizeDropDownList();
        softAssert.assertTrue(dropDownListPage.isDownArrow());
        softAssert.assertTrue(dropDownListPage.isHiddenDropDownListElements());
        softAssert.assertAll();
    }
}
