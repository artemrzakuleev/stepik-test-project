package ui.dropDownLIst;

import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import page.DropDownListPage;
import ui.BaseUITest;

public class MaximizeDropDownList extends BaseUITest {
    private static final Logger logger= LoggerFactory.getLogger(MaximizeDropDownList.class);
    private DropDownListPage dropDownListPage;

    @Test
    @Severity(SeverityLevel.MINOR)
    public void maximizeDropDownList() {
        logger.info("Testing maximize of drop-down list");
        SoftAssert softAssert=new SoftAssert();
        dropDownListPage=new DropDownListPage(driver)
                .openPage()
                .moveToList()
                .maximizeDropDownList()
                .minimizeDropDownList();
        softAssert.assertTrue(dropDownListPage.isUpArrow());
        softAssert.assertTrue(dropDownListPage.isVisibleDropDownListElements());
    }
}
