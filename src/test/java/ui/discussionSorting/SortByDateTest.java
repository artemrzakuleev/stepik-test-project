package ui.discussionSorting;

import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.DiscussionSortPage;
import ui.BaseUITest;

public class SortByDateTest extends BaseUITest {
    private static final Logger logger= LoggerFactory.getLogger(SortByDateTest.class);
    private DiscussionSortPage discussionSortPage;

    @Test
    @Severity(SeverityLevel.MINOR)
    public void sortByDateTest()  {
        discussionSortPage=new DiscussionSortPage(driver)
                .openPage()
                .setSorting("by_date")
                .expandDiscussion();
        Assert.assertTrue(discussionSortPage.isSortingByDate());
    }
}
