package ui.discussionSorting;

import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;
import page.DiscussionSortPage;
import ui.BaseUITest;

public class SortByActiveTest extends BaseUITest {
    private static final Logger logger= LoggerFactory.getLogger(SortByActiveTest.class);
    private DiscussionSortPage discussionSortPage;

    @Test
    @Severity(SeverityLevel.MINOR)
    public void sortByActiveTest()  {
        discussionSortPage=new DiscussionSortPage(driver)
                .openPage()
                .setSorting("active")
                .expandDiscussion();
        Assert.assertTrue(discussionSortPage.isSortingByActive());
    }
}