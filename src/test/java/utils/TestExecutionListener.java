package utils;

import io.qameta.allure.Attachment;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestListener;
import org.testng.ITestResult;
import page.RegistrationPage;
import ui.BaseUITest;

public class TestExecutionListener implements ITestListener {
    private static final Logger logger= LoggerFactory.getLogger(TestExecutionListener.class);
    private WebDriver driver;
    @SuppressWarnings("UnusedReturnValue")
    @Attachment(value = "Page screenshot", type = "image/png",fileExtension = ".png")
    public byte[] saveScreenshot()  {
        logger.info("Capturing screenshot of browser");
        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    }
    @Override
    public void onTestFailure(ITestResult result) {
        logger.info("Test failed.");
        try {
            this.driver = ((BaseUITest) result.getInstance()).getDriver();
            saveScreenshot();
        }
        /**
         * Test result can include tests of API. API tests don't support screen capture. Try\catch ignore exceptions to cast API tests to UI tests;
         */
        catch (ClassCastException e)    {
        }
    }
}