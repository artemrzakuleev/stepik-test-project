package utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ui.registration.TermsLink;

import java.util.ResourceBundle;

public class TestDataReader {
    private static final Logger logger= LoggerFactory.getLogger(TestDataReader.class);
    private static final ResourceBundle resourceBundle=ResourceBundle.getBundle(System.getProperty("environment","qa"));
    public static String getTestData(String key)    {
        logger.info("Reading value of '{}' from properties '{}' ",key,System.getProperty("environment","qa"));
        try {
            return resourceBundle.getString(key);
        }
        catch (Exception e) {
            logger.error(String.valueOf(e));
            throw e;
        }
    }
}
