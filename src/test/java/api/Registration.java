package api;

import com.google.gson.JsonObject;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import utils.TestDataReader;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

public class Registration {
    Response getCookies;
    RequestSpecification requestSpecification;
    /**
     * Получаем cookies для sessionID и csrfToken. Данные параметры необходимы для обхода CRSF защиты.
     */
    @BeforeMethod
    public void setup() {
        RestAssured.baseURI="https://stepik.org";
        RestAssured.basePath="/api/users";
        getCookies=given()
                .header("Referer","https://stepik.org/catalog")
                .get(baseURI+"/catalog?auth=registration")
                .then()
                .extract()
                .response();
        requestSpecification=given()
                .filter(new AllureRestAssured())
                .contentType(ContentType.JSON)
                .header("Referer","https://stepik.org/catalog?auth=registration")
                .header("X-CSRFToken", getCookies.cookie("csrftoken"))
                .header("Accept-Language","ru-RU,ru;q=0.9")
                .cookie("sessionid",getCookies.cookie("sessionid"))
                .cookie("csrftoken",getCookies.cookie("csrftoken"));
    }
    @Step
    @Severity(SeverityLevel.BLOCKER)
    @Test (description = "Попытка регистрации с пустыми полями ввода")
    public void emptyInputTest()    {
    String expectedMessageEn="This field is required.";
    String expectedMessageRu="Обязательное поле.";
    int expectedStatusCode=400;
    String email= TestDataReader.getTestData("registration.emptyInput.email");
    String password=TestDataReader.getTestData("registration.emptyInput.password");
    String fullName=TestDataReader.getTestData("registration.emptyInput.fullName");
    JsonObject requestParams = new JsonObject();
    JsonObject user = new JsonObject();
    user.addProperty("first_name", fullName);
    user.addProperty("last_name", "");
    user.addProperty("email", email);
    user.addProperty("password", password);
    requestParams.add("user",user);
    String requestBody=requestParams.toString();
    given()
            .spec(requestSpecification)
            .body(requestBody)
            .post()
            .then()
            .log().all()
            .statusCode(expectedStatusCode)
            .body("email[0]",oneOf(expectedMessageEn,expectedMessageRu));
}
    @Step
    @Severity(SeverityLevel.BLOCKER)
    @Test (description = "Проверка регистрации с некорректным форматом email - отсутствует доменная часть и @")
    public void atIsMissingTest()   {
        String expectedMessageEn="Enter a valid email address.";
        String expectedMessageRu="Введите правильный адрес электронной почты.";
        int expectedStatusCode=400;
        String email= TestDataReader.getTestData("registration.atIsMissing.email");
        String password=TestDataReader.getTestData("registration.atIsMissing.password");
        String fullName=TestDataReader.getTestData("registration.atIsMissing.fullName");
        JsonObject requestParams = new JsonObject();
        JsonObject user = new JsonObject();
        user.addProperty("first_name", fullName);
        user.addProperty("last_name", "");
        user.addProperty("email", email);
        user.addProperty("password", password);
        requestParams.add("user", user);
        String requestBody=requestParams.toString();
        given()
                .spec(requestSpecification)
                .body(requestBody)
                .post()
                .then()
                .log().all()
                .statusCode(expectedStatusCode)
                .body("email[0]",oneOf(expectedMessageEn,expectedMessageRu));
    }
    @Step
    @Severity(SeverityLevel.BLOCKER)
    @Test (description =" тест регистрации с уже существующим аккаунтом")
    public void existedAccountTest() {
        String expectedMessageEn="User with this email address already exists.";
        String expectedMessageRu="Пользователь с таким e-mail адресом уже существует.";
        int expectedStatusCode=400;
        String email= TestDataReader.getTestData("registration.existedAccount.email");
        String password=TestDataReader.getTestData("registration.existedAccount.password");
        String fullName=TestDataReader.getTestData("registration.existedAccount.fullName");
        JsonObject requestParams = new JsonObject();
        JsonObject user = new JsonObject();
        user.addProperty("first_name", fullName);
        user.addProperty("last_name", "");
        user.addProperty("email", email);
        user.addProperty("password", password);
        requestParams.add("user", user);
        String requestBody = requestParams.toString();
        given()
                .spec(requestSpecification)
                .body(requestBody)
                .post()
                .then()
                .log().all()
                .statusCode(expectedStatusCode)
                .body("email[0]",oneOf(expectedMessageEn,expectedMessageRu));
    }

    @Step
    @Severity(SeverityLevel.BLOCKER)
    @Test (description = "Проверка регистрации с некорректным форматом email - отсутствует доменная часть")
    public void domainIsMissingTest()   {
        String expectedMessageEn="Enter a valid email address.";
        String expectedMessageRu="Введите правильный адрес электронной почты.";
        int expectedStatusCode=400;
        String email= TestDataReader.getTestData("registration.domainIsMissing.email");
        String password=TestDataReader.getTestData("registration.domainIsMissing.password");
        String fullName=TestDataReader.getTestData("registration.domainIsMissing.fullName");
        JsonObject requestParams = new JsonObject();
        JsonObject user = new JsonObject();
        user.addProperty("first_name", fullName);
        user.addProperty("last_name", "");
        user.addProperty("email", email);
        user.addProperty("password", password);
        requestParams.add("user", user);
        String requestBody=requestParams.toString();
        given()
                .spec(requestSpecification)
                .body(requestBody)
                .post()
                .then()
                .log().all()
                .statusCode(expectedStatusCode)
                .body("email[0]",oneOf(expectedMessageEn,expectedMessageRu));
    }
}
