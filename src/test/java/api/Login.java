package api;

import com.google.gson.JsonObject;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import io.qameta.allure.Step;
import io.qameta.allure.restassured.AllureRestAssured;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import utils.TestDataReader;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.oneOf;

public class Login {
    Response getCookies;
    RequestSpecification requestSpecification;
    /**
     * Получаем cookies для sessionID и csrfToken. Данные параметры необходимы для обхода CRSF защиты.
     */

    @BeforeMethod
    public void setup() {
        RestAssured.baseURI="https://stepik.org";
        RestAssured.basePath="/api/users/login";
        getCookies=given()
                .header("Referer","https://stepik.org/catalog")
                .get(baseURI+"/catalog?auth=login")
                .then()
                .log().headers()
                .extract()
                .response();
        requestSpecification=given()
                .filter(new AllureRestAssured())
                .contentType(ContentType.JSON)
                .header("Referer","https://stepik.org/catalog?auth=login")
                .header("X-CSRFToken", getCookies.cookie("csrftoken"))
                .header("Accept-Language","ru-RU,ru;q=0.9")
                .cookie("sessionid",getCookies.cookie("sessionid"))
                .cookie("csrftoken",getCookies.cookie("csrftoken"));
    }
    @Step
    @Severity(SeverityLevel.BLOCKER)
    @Test (description = "тест авторизации с корректным логином и некорректным паролем")
    public void invalidPasswordLoginTest(){
        String expectedMessageEn="The e-mail address and/or password you specified are not correct.";
        String expectedMessageRu="E-mail адрес и/или пароль не верны.";
        int expectedStatusCode=401;
        String email= TestDataReader.getTestData("login.invalidPassword.email");
        String password=TestDataReader.getTestData("login.invalidPassword.password");
        JsonObject requestParams=new JsonObject();
        requestParams.addProperty("email",email);
        requestParams.addProperty("password",password);
        String requestBody=requestParams.toString();
        given()
                .spec(requestSpecification)
                .body(requestBody)
                .post()
                .then()
                .log().all()
                .statusCode(expectedStatusCode)
                .body("detail",oneOf(expectedMessageEn,expectedMessageRu));
    }

    @Step
    @Severity(SeverityLevel.BLOCKER)
    @Test (description = "тест авторизации с существующим аккаунтом")
    public void successfulLoginTest() {
        int expectedStatusCode=204;
        String email= TestDataReader.getTestData("login.successfulLogin.email");
        String password=TestDataReader.getTestData("login.successfulLogin.password");
        JsonObject requestParams=new JsonObject();
        requestParams.addProperty("email",email);
        requestParams.addProperty("password",password);
        String requestBody=requestParams.toString();
        given()
                .spec(requestSpecification)
                .body(requestBody)
                .post()
                .then()
                .log().all()
                .statusCode(expectedStatusCode);
    }

    @Step
    @Severity(SeverityLevel.BLOCKER)
    @Test ( description = "тест авторизации с несуществующим аккаунтом")
    public void nonExistedAccountTest() {
        String expectedMessageEn="The e-mail address and/or password you specified are not correct.";
        String expectedMessageRu="E-mail адрес и/или пароль не верны.";
        int expectedStatusCode=401;
        String email= TestDataReader.getTestData("login.nonExistedAccount.email");
        String password=TestDataReader.getTestData("login.nonExistedAccount.password");
        JsonObject requestParams=new JsonObject();
        requestParams.addProperty("email",email);
        requestParams.addProperty("password",password);
        String requestBody=requestParams.toString();
        given()
                .spec(requestSpecification)
                .body(requestBody)
                .post()
                .then()
                .log().all()
                .statusCode(expectedStatusCode)
                .body("detail",oneOf(expectedMessageEn,expectedMessageRu));
    }
}
