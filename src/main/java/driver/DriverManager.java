package driver;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.opera.OperaDriver;
import org.openqa.selenium.opera.OperaOptions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DriverManager {
    private final static Logger logger = LoggerFactory.getLogger(DriverManager.class);
    private WebDriver driver;
    private DriverManager(){}

    public static WebDriver getDriver()  {
        WebDriver driver=null;
            switch (System.getProperty("browser","chrome").toLowerCase())  {
                case "firefox": {
                    WebDriverManager.firefoxdriver().setup();
                    FirefoxOptions options=new FirefoxOptions();
                    if (System.getProperty("headless","false").contains("true")) {
                        options.setHeadless(true);
                    }
                    options.addArguments("--lang=ru");
                    driver=new FirefoxDriver(options);
                } break;
                case "opera":   {
                    WebDriverManager.operadriver().setup();
                    OperaOptions options=new OperaOptions();
                    options.addArguments("--lang=ru");
                    driver=new OperaDriver();
                } break;
                case "firefox-in-docker":   {
                    FirefoxProfile profile=new FirefoxProfile();
                    profile.setPreference("intl.accept_languages", "ru-RU");
                    FirefoxOptions options=new FirefoxOptions();
                    options.setProfile(profile);
                    driver=WebDriverManager.firefoxdriver().browserInDocker().dockerLang("ru").capabilities(options).create();
                }   break;
                case "chrome-in-docker":    {
                    ChromeOptions options=new ChromeOptions();
                    options.addArguments("wdm.dockerLang","ru");
                    options.addArguments("--remote-allow-origins=*");
                    driver=WebDriverManager.chromedriver().browserInDocker().dockerLang("ru").create();
                }   break;
                default:    {
                    WebDriverManager.chromedriver().setup();
                    ChromeOptions options=new ChromeOptions();
                    options.addArguments("--remote-allow-origins=*");
                    options.addArguments("--lang=ru");
                    if (System.getProperty("headless","false").contains("true")) {
                        options.setHeadless(true);
                    }
                    driver=new ChromeDriver(options);
                }
            }
        return driver;
    }
    public void closeDriver()   {
        driver.quit();
    }
}
