package page;

import io.qameta.allure.Description;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;

public class LanguagePage {
    private static final Logger logger= LoggerFactory.getLogger(LanguagePage.class);
    private final WebDriver driver;
    private final Duration duration=Duration.ofSeconds(15);
    private final String page_URL="https://stepik.org/catalog";
    private final WebDriverWait driverWait;
    public LanguagePage(WebDriver driver)    {
        this.driver=driver;
        PageFactory.initElements(this.driver,this);
        driverWait=new WebDriverWait(driver,duration);
    }
    /**
     * Локатор кнопки смены языка
     */
    @FindBy(xpath = "//button[@class='navbar__submenu-toggler st-button_style_none']")
    private WebElement languageButton;
    /**
     * Локатор кнопки поиска ( для проверки переключения языка)
     */
    @FindBy(xpath = "//button[@class='button_with-loader search-form__submit']")
    private WebElement searchButton;

    /**
     * Метод для проверки соответствия текста кнопки Поиска ожидаемому языку
     */
    @Step
    @Description(useJavaDoc = true)
    public boolean checkEqualTextInSearchButtonToExpected(String textOfSearchButton)
    {
        logger.info("Checking is equal language of text in search button to expected language");
        driverWait.
                until(ExpectedConditions.textToBePresentInElement(searchButton,textOfSearchButton));
        return textOfSearchButton.equals(searchButton.getText());
    }
    /**
     * Метод для Переключения языка
     */
    @Step
    @Description(useJavaDoc = true)
    public LanguagePage changeLanguage(String language) {
        driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
        logger.info("Waiting until language button is clickable");
        driverWait
                .until(ExpectedConditions.elementToBeClickable(languageButton))
                .click();
        logger.info("Changing language to {}",language);
        driver.findElement(By.xpath("//button[text()='"+language+"']"))
                .click();
        return this;
    }
    /**
     * Открыть главную страницу
     */
    @Step
    @Description(useJavaDoc = true)
    public LanguagePage openPage()  {
        logger.info("Opening page {}",page_URL);
        driver.get(page_URL);
        return  this;
    }

}
