package page;

import io.qameta.allure.Description;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;

public class DropDownListPage {
    private static final Logger logger= LoggerFactory.getLogger(DropDownListPage.class);
    private final WebDriver driver;
    private final String pageUrl="https://stepik.org/course/187/promo";
    private final Duration duration= Duration.ofSeconds(15);
    private final WebDriverWait driverWait;
    private final By elementsInDropDownList=By.xpath("//span[@class='toc-promo__section-widget-title' and text()='Введение в Java']/parent::div/parent::div/descendant::li");

    /**
     *Локатор первого элемента в списке курса
     */
    @FindBy(xpath="//span[@class='toc-promo__section-widget-title' and text()='Введение в Java']/parent::div/parent::div")
    private WebElement promoSection;
    /**
     * Локатор иконки стрелки в первом элемента списка
     */
    @FindBy(xpath="//span[@class='toc-promo__section-widget-title' and text()='Введение в Java']/parent::div/parent::div/descendant::span[contains(@class,'arrow_icon')]")
    private WebElement arrowIcon;
    /**
     * Список элементов в списке
     */
    @FindBy(xpath = "//span[@class='toc-promo__section-widget-title' and text()='Введение в Java']/parent::div/parent::div/descendant::li")
    private WebElement firstElementInDropDownList;
    /**
     * Заголовок списка
     */
    @FindBy(xpath = "//section[@data-qa='course-promo__course-content']")
    private WebElement listHeader;
    /**
     * Заголовок "Программа курса"
     * Необходим для перемещения к выпадающему списку из за поэтапной загрузки страницы
     */
    @FindBy(xpath = "//h2[@id='toc']")
    private WebElement courseProgram;
    /**
     * Раздел "Наши преподаватели"
     */
    @FindBy(xpath = "//section[@data-qa='course-promo__instructors']")
    private WebElement ourInstructors;
    /**
     * Раздел "Для кого этот курс"
     */
    @FindBy(xpath = "//section[@data-qa='course-promo__target-audience']")
    private WebElement audience;
    /**
     * Конструктор с инициализацией полей класса
     */
    public DropDownListPage(WebDriver driver) {
        PageFactory.initElements(driver,this);
        this.driver =driver;
        driverWait=new WebDriverWait(driver,duration);
    }
    /**
     * Открытие страницы с выпадающим списком
     * @return
     */
    public DropDownListPage openPage()  {
        logger.info("Opening page {}",pageUrl);
        driver.get(pageUrl);
        return this;
    }
    /**
     * Свернуть список
     */
    @Step
    @Description(useJavaDoc = true)
    public DropDownListPage minimizeDropDownList()  {
        logger.info("Minimize drop-down list");
        driverWait.
                until(ExpectedConditions.elementToBeClickable(arrowIcon)).
                click();
        return this;
    }
    /**
     * Передвижение к выпадающему списку
     * Firefox driver имеет issue с actions.moveTo
     */
    @Step
    @Description(useJavaDoc = true)
    public DropDownListPage moveToList() {
        logger.info("Moving to drop-down list");
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", listHeader);
        return this;
    }
    /**
     * Развернуть список
     */
    @Step
    @Description(useJavaDoc = true)
    public DropDownListPage maximizeDropDownList()  {
        logger.info("Maximize drop-down list");
        driverWait.
                until(ExpectedConditions.elementToBeClickable(arrowIcon)).
                click();
        return this;
    }
    /**
     * Проверка соответствия типа стрелки - стрелке вверх
     */
    @Step
    @Description
    public boolean isUpArrow()  {
        logger.info("Checking is arrow oriented up");
        driverWait.
                until(ExpectedConditions.attributeContains(arrowIcon,"class","up-arrow_icon"));

        return arrowIcon.getAttribute("class").contains("up-arrow_icon");
    }
    /**
     * Проверка соответствия типа стрелки - стрелке вниз
     */
    @Step
    @Description
    public boolean isDownArrow()  {
        logger.info("Checking is arrow oriented down");
        driverWait.
                until(ExpectedConditions.attributeContains(arrowIcon,"class","down-arrow_icon"));

        return arrowIcon.getAttribute("class").contains("down-arrow_icon");
    }

    /**
     * Видны ли элементы выпадающего списка
     */
    @Step
    @Description
    public boolean isVisibleDropDownListElements()   {
        logger.info("Checking is visible elements of drop-down list");
        return driverWait.
                until(ExpectedConditions.presenceOfElementLocated(elementsInDropDownList))
                .isDisplayed();
    }
    @Step
    @Description
    public boolean isHiddenDropDownListElements()   {
        logger.info("Checking is hidden elements of drop-down list");
        return !driverWait.
                until(ExpectedConditions.presenceOfElementLocated(elementsInDropDownList))
                .isDisplayed();
    }
}
