package page;

import io.qameta.allure.Description;
import io.qameta.allure.Step;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;

public class SearchPage {
    private static final Logger logger= LoggerFactory.getLogger(SearchPage.class);
    private final WebDriver driver;
    private final Duration duration=Duration.ofSeconds(15);
    private final String pageURL="https://stepik.org/catalog";
    private final WebDriverWait driverWait;
    public SearchPage(WebDriver driver) {
        PageFactory.initElements(driver,this);
        this.driver=driver;
        driverWait=new WebDriverWait(driver,duration);
    }
    /**
     * Локатор поля ввода для поиска
     */
    @FindBy(xpath = "//input[@class='navbar__search-input st-input st-input_border_none']")
    private WebElement searchInput;
    /**
     * Локатор кнопки поиска
     */
    @FindBy(xpath = "//button[@class='button_with-loader search-form__submit']")
    private WebElement searchButton;
    /**
     * Локатор каталога результатов поиска
     */
    @FindBy(xpath="//div[@data-list-type='search-results']")
    private WebElement searchResults;
    /**
     * Ввести text в поле ввода для поиска
     * @param text текст для поиска
     */
    @Step
    @Description(useJavaDoc = true)
    public SearchPage enterTextInSearchInput(String text)   {
        logger.info("Entering text of search request to input");
        driverWait
                .until(ExpectedConditions.elementToBeClickable(searchInput))
                .sendKeys(text);
        searchInput.sendKeys(Keys.ENTER);
        return this;
    }
    /**
     * Кликнуть по кнопке "Искать"
     */
    public SearchPage clickButtonSearch()   {
        logger.info("Click button search");
        driverWait
                .until(ExpectedConditions.elementToBeClickable(searchButton))
                .click();
        return this;
    }
    /**
     * Открыть страницу с поиском
     */
    @Step
    @Description(useJavaDoc = true)
    public SearchPage openPage()  {
        logger.info("Opening page {}",pageURL);
        driver.get(pageURL);
        driverWait
                .until(ExpectedConditions.visibilityOf(searchInput));
        return this;
    }

    /**
     * Проверка наличия текста text в результатах поисках
     * @param text введенный текст в строку поиска
     */
    @Step
    @Description(useJavaDoc = true)
    public boolean isSearchResultsContainsText(String text)    {
        String searchUrl=new StringBuilder("https://stepik.org/catalog/search?q=")
                .append(text)
                .toString();
        String textOfSearchXpath=new StringBuilder("//a[contains(@aria-label,'")
                .append(text)
                .append("')]")
                .toString();
        logger.info("Waiting until opening page with results of search");
        driverWait
                .until(ExpectedConditions.urlToBe(searchUrl));
        logger.info("Checking that page content contain search request");
        return driverWait
                .until(ExpectedConditions.visibilityOfElementLocated(By.xpath(textOfSearchXpath)))
                .isDisplayed();
    }
}
