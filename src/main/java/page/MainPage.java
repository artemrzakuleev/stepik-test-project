package page;

import io.qameta.allure.Description;
import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;

public class MainPage {
    private static final Logger logger=LoggerFactory.getLogger(MainPage.class);
    private final WebDriver driver;
    private final Duration duration=Duration.ofSeconds(15);
    private final String pageURl="https://stepik.org/catalog";
    private final WebDriverWait driverWait;
    public MainPage(WebDriver driver)    {
        PageFactory.initElements(driver,this);
        this.driver =driver;
        driverWait=new WebDriverWait(driver,duration);
    }
    /**
     * Локатор кнопки профиля
     */
    @FindBy(xpath = "//div[@class='drop-down drop-down-menu ember-view navbar__profile']")
    private WebElement profile;
    /**
     * Метод проверки авторизовался ли пользователь
     */
    @Step
    @Description(useJavaDoc = true)
    public boolean isUserLogged()   {
        logger.info("Checking is user logged");
        return driverWait.
                until(ExpectedConditions.visibilityOf(profile)).
                isDisplayed();
    }
}
