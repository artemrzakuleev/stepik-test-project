package page;

import io.qameta.allure.Description;
import io.qameta.allure.Step;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;

public class LoginPage {
    private static final Logger logger= LoggerFactory.getLogger(LoginPage.class);
    private final WebDriver driver;
    private final WebDriverWait driverWait;
    private final Duration duration=Duration.ofSeconds(15);
    private final String pageURL="https://stepik.org/catalog?auth=login";
    /**
     * Конструктор с инициализацией полей класса
     */
    public LoginPage(WebDriver driver) {
        PageFactory.initElements(driver,this);
        this.driver =driver;
        driverWait=new WebDriverWait(driver,duration);
    }
    /**
     * Локатор поля ввода Логина
     */
    @FindBy(xpath = "//input[@name='login']")
    private WebElement loginField;
    /**
     * Локатор поля ввода пароля
     */
    @FindBy(xpath ="//input[@name='password']")
    private WebElement passwordField;
    /**
     * Локатор кнопки Войти в форме логина
     */
    @FindBy(xpath = "//button[@class='sign-form__btn button_with-loader ']")
    private WebElement loginButton;
    /**
     * Локатор предупреждения о некорректных данных: email или пароль
     */
    @FindBy(xpath="//li[text()='E-mail адрес и/или пароль не верны.']")
    private WebElement invalidLoginAlert;
    /**
     * Локатор формы логина
     */
    @FindBy(xpath="//form[@id='login_form']")
    private WebElement loginForm;
    /**
     * Локатор Ссылки "Напомнить пароль" для сброса пароля
     */
    @FindBy(xpath="//a[@class='ember-view link_no-static-line']")
    private WebElement remindPassword;
    /**
     * Форма сброса пароля
     */
    @FindBy(xpath = "//form[@id='reset_form']")
    private WebElement resetPasswordForm;

    /**
     *  Открыть страницу логина
     */
    @Step
    @Description(useJavaDoc = true)
    public LoginPage openPage()  {
        logger.info("Opening page {}",pageURL);
        driver.get(pageURL);
        driverWait.
                until(ExpectedConditions.visibilityOf(loginForm));
        return this;
    }
    /**
     *Метод для ввода логина
     */
    @Step()
    @Description(useJavaDoc = true)
    public LoginPage inputLogin(String email)    {
        logger.info("Entering email from test data");
        loginField.sendKeys(email);
        return this;
    }
    /**
     * Метод для ввода пароля
     */
    @Step
    @Description(useJavaDoc = true)
    public LoginPage inputPassword(String password) {
        logger.info("Entering password from test data");
        passwordField.sendKeys(password);
        return this;
    }
    /**
     * Метод для удачной попытки логина пользователем.
     * @return Главная страница с зарегистрированным пользователем
     */
    public MainPage logUser(){
        logger.info("Logging test user");
        loginButton.click();
        return new MainPage(driver);
    }

    /**
     * Метод для удачного логина пользователя для страницы удаления
     * @return Страница с удалением пользователя
     */
    public DeleteAccountPage logUserToDeleteUser(){
        logger.info("Logging user to delete user");
        loginButton.click();
        return new DeleteAccountPage(driver);
    }


    /**
     * Метод для неудачной попытки логина пользователем.
     * @return Текущая страница логина
     */
    public LoginPage tryLogUser()   {
        logger.info("Trying log user");
        loginButton.click();
        return this;
    }
    /**
     * Метод для проверки наличия формы логина на экране
     */
    @Step
    @Description(useJavaDoc = true)
    public boolean isLoginFormVisible(){
        logger.info("Checking is login form visible");
        return driverWait.
                until(ExpectedConditions.visibilityOf(loginForm)).
                isDisplayed();
    }
    /**
     *Метод для получения алерта о неправильном пароле имени
     */
    @Step
    @Description
    public boolean isVisibleInvalidLoginAlert(){
        logger.info("Checking is invalid login alert visible");
        return driverWait.
                until(ExpectedConditions.visibilityOf(invalidLoginAlert)).
                isDisplayed();
    }

    /**
     * Метод для клика по кнопке "Напомнить пароль"
     */
    @Step
    @Description(useJavaDoc = true)
    public LoginPage clickRemindPassword()   {
        logger.info("Clicking remind password");
        driverWait
                .until(ExpectedConditions.visibilityOf(remindPassword))
                .click();
        return this;
    }

    /**
     * Проверка видимости формы сброса пароля
     * @return true, если форма видна
     */
    @Step
    @Description(useJavaDoc = true)
    public boolean isResetFormVisible() {
        logger.info("Checking is reset form visible");
        return driverWait
                .until(ExpectedConditions.visibilityOf(resetPasswordForm))
                .isDisplayed();
    }
    /**
     * Изменение типа поля ввода для email с email на текст
     * Return текущая страница логина
     */
    @Step
    @Description(useJavaDoc = true)
    public LoginPage changeTypeOfEmailField()   {
        logger.info("Changing type of email field");
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("document.getElementsByName('login')[0].setAttribute('type','text');");
        return this;
    }
}
