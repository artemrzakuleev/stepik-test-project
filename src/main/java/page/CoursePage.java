package page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.time.Duration;

public class CoursePage {
    private final WebDriver driver;
    private final Duration duration=Duration.ofSeconds(15);
    /**
     * Конструктор с инициализацией полей класса
     * @param driver
     */
    public CoursePage(WebDriver driver) {
        PageFactory.initElements(driver,this);
        this.driver =driver;
    }
    /**
     * Локатор с именем курса
     */
    @FindBy(xpath="//h1[@class='course-promo__header']")
    private WebElement courseName;
    /**
     * Методя для получения имени курса
     * @return courseName
     */
    public String getCourseName(){
        return courseName.getText();
    }

}
