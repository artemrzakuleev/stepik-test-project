package page;

import io.qameta.allure.Description;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;

public class DeleteAccountPage {
    private static final Logger logger=LoggerFactory.getLogger(DeleteAccountPage.class);
    private final WebDriver driver;
    private final Duration duration=Duration.ofSeconds(10);
    private final WebDriverWait driverWait;
    private final String pageURL="https://stepik.org/users/delete-account/";
    /**
     * Конструктор с инициализацией полей класса
     */
    public DeleteAccountPage(WebDriver driver)   {
        PageFactory.initElements(driver,this);
        this.driver = driver;
        driverWait=new WebDriverWait(driver,duration);
    }
    /**
     * Локатор поля ввода пароля
     */
    @FindBy(xpath = "//input[@type='email']")
    private WebElement email;
    /**
     * Локатор поля ввода пароля
     */
    @FindBy(xpath="//input[@type='password']")
    private WebElement password;
    /**
     * Локатор кнопки "Delete!"
     */
    @FindBy(xpath ="//input[@class='button alert']")
    private WebElement deleteButton;
    /**
     * Метод для ввода пароля
     */
    private DeleteAccountPage inputPassword(String passwordValue) {
        logger.info("Entering password from test data");
        driverWait
                .until(ExpectedConditions.visibilityOf(password))
                .sendKeys(passwordValue);
        return this;
    }
    /**
     * Метод для осуществления клика по кнопке "Delete!"
     */
    private DeleteAccountPage clickDeleteButton(){
        logger.info("Clicking button delete account");
        driverWait
                .until(ExpectedConditions.visibilityOf(deleteButton))
                .click();
        return this;
    }

    /**
     * Метод для открытия страницы удаления аккаунта
     */
    public DeleteAccountPage openPage(){
        logger.info("Opening page {}",pageURL);
        driver.get(pageURL);
        driver.get(pageURL);
        driverWait
                .until(ExpectedConditions.titleContains("Delete account"));
        return this;
    }
    /**
     * Метод для клика по всплывающему окну подтверждения удаления аккаунта
     */
    @Description(useJavaDoc = true)
    private  DeleteAccountPage clickAcceptDeletingAccount()    {
        logger.info("Clicking accept in alert");
        driverWait
                .until(ExpectedConditions.alertIsPresent());
        driver.switchTo().alert().accept();
        return this;
    }
    public DeleteAccountPage deleteAccount(String email,String password) {
        inputPassword(password);
        clickDeleteButton();
        clickAcceptDeletingAccount();
        return this;
    }



}
