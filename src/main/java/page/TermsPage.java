package page;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;

public class TermsPage {
    private static final Logger logger= LoggerFactory.getLogger(PrivacyPage.class);
    private final WebDriver driver;
    private final Duration duration=Duration.ofSeconds(15);
    private final String titleName="Условия использования — Stepik";
    private final WebDriverWait driverWait;
    public TermsPage(WebDriver driver)    {
        this.driver =driver;
        driverWait=new WebDriverWait(driver,duration);
    }
    public boolean isCurrentPageTerms()    {
        logger.info("Checking is current page terms");
        return driverWait
                .until(ExpectedConditions.titleIs(titleName));
    }
}
