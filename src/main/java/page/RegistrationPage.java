package page;

import io.qameta.allure.Description;
import io.qameta.allure.Step;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;

public class RegistrationPage {
    private static final Logger logger= LoggerFactory.getLogger(RegistrationPage.class);
    private final WebDriver driver;
    private final Duration duration=Duration.ofSeconds(15);
    private final WebDriverWait driverWait;
    private final String pageURL="https://stepik.org/catalog?auth=registration";
    /**
     * Конструктор с инициализацией полей класса
     */
    public RegistrationPage(WebDriver driver) {
        PageFactory.initElements(driver,this);
        this.driver =driver;
        driverWait=new WebDriverWait(driver,duration);
    }
    /**
     * Форма регистрации
     */
    @FindBy(xpath = "//form[@id='registration_form']")
    private WebElement registrationForm;
    /**
     * Локатор поля ввода имени
     */
    @FindBy(xpath = "//input[@name='full_name']")
    private WebElement fullName;
    /**
     * Локатор поля ввода email
     */
    @FindBy(xpath ="//input[@name='email']" )
    private WebElement email;
    /**
     * Локатор поля ввода пароля
     */
    @FindBy(xpath ="//input[@name='password']")
    private WebElement password;
    /**
     * Локатор кнопки Регистрация в форме регистрации
     */
    @FindBy(xpath = "//button[@class='sign-form__btn button_with-loader ']")
    private WebElement registrationButton;
    /**
     * Локатор алерта о уже существующем пользователе
     */
    @FindBy(xpath="//li[text()='Пользователь с таким e-mail адресом уже существует.']")
    private WebElement existedAccountAlert;
    /**
     * Локатор алерта о некорректном емейле
     */
    @FindBy(xpath="//li[text()='Введите правильный адрес электронной почты.']")
    private WebElement invalidEmailAddressAlert;
    /**
     * Ссылка "Правила"
     */
    @FindBy(xpath = "//form[@id='registration_form']//a[@href='https://welcome.stepik.org/ru/terms']")
    private WebElement termsLink;
    /**
     * Ссылка "Конфиденциальность"
     */
    @FindBy(xpath="//form[@id='registration_form']//a[contains(@href,'https://welcome.stepik.org/ru/privacy')]")
    private WebElement privacyLink;

    /**
     * Метод для ввода ФИО пользователя
     */
    @Step
    @Description(useJavaDoc = true)
    public RegistrationPage inputName(String fullNameValue)  {
        logger.info("Entering name from test data");
        fullName.sendKeys(fullNameValue);
        return this;
    }

    /**
     *Метод для ввода email
     */
    @Step()
    @Description(useJavaDoc = true)
    public RegistrationPage inputEmail(String emailValue)    {
        logger.info("Entering email from test data");
        email.sendKeys(emailValue);
        return this;
    }

    /**
     * Метод для ввода пароля
     */
    @Step
    @Description(useJavaDoc = true)
    public RegistrationPage inputPassword(String passwordValue) {
        logger.info("Entering password from test data");
        password.sendKeys(passwordValue);
        return this;
    }

    /**
     * Метод для неудачной попытки регистрации пользователя
     * @return текущую страницу
     */
    public RegistrationPage tryRegisterUser(){
        logger.info("Trying register user");
        registrationButton.click();
        return this;
    }

    /**
     * Метод для  регистрации пользователя
     * @return страницу каталога уже с новозарегистрированным пользователем
     */
    public MainPage registerUser()  {
        logger.info("Registering test user");
        registrationButton.click();
        return new MainPage(driver);
    }

    /**
     * Метод для нахождения алерта о уже существующем пользователе
     * @return Главную страницу с зарегистрированным пользователем
     */
    @Step
    @Description(useJavaDoc = true)
    public boolean isVisibleExistedAccountAlert() {
        logger.info("Checking is visible alert about existing account");
        return new WebDriverWait(driver,duration).
                until(ExpectedConditions.visibilityOf(existedAccountAlert)).
                isDisplayed();
    }

    /**
     * Метод для нахождения алерта о некорректном емейле
     */
    @Step
    @Description(useJavaDoc = true)
    public boolean isVisibleInvalidEmailAddressAlert(){
        logger.info("Checking is visible alert about invalid email");
        return new WebDriverWait(driver,duration).
                until(ExpectedConditions.visibilityOf(invalidEmailAddressAlert)).
                isDisplayed();
    }

    /**
     * Открыть страницу регистрации
     */
    @Step
    @Description(useJavaDoc = true)
    public RegistrationPage openPage()  {
        logger.info("Opening page {}",pageURL);
        driver.get(pageURL);
        new WebDriverWait(driver,duration).until(ExpectedConditions.visibilityOf(registrationForm));
        return this;
    }

    /**
     * Проверка наличия формы регистрации на экране
     */
    @Step
    @Description(useJavaDoc = true)
    public boolean isRegistrationFormVisible()  {
        logger.info("Checking is registration form visible");
        return new WebDriverWait(driver,duration).
                until(ExpectedConditions.visibilityOf(registrationForm)).
                isDisplayed();
    }

    /**
     * Клик по кнопке "Правила"
     * @return текущая страница
     */
    @Step
    @Description(useJavaDoc = true)
    public RegistrationPage clickTerms()    {
        logger.info("Clicking Terms link");
        driverWait
                .until(ExpectedConditions.visibilityOf(termsLink))
                .click();
        return this;
    }

    /**
     * Смена вкладки для ссылки на "Правила". Данная ссылка открывается во второй вкладке
     * @return Страница с правилами
     */
    @Step
    @Description(useJavaDoc = true)
    public TermsPage switchTabToTermsPage() {
        logger.info("Switching tab in browser to second tab with terms");
        List tabs = new ArrayList (driver.getWindowHandles());
        logger.info("tabs id: "+tabs.toString());
        driver.switchTo().window((String) tabs.get(1));
        return new TermsPage(driver);
    }

    /**
     * Клик по кнопке "Конфиденциальность"
     * @return текущая страница
     */
    @Step
    @Description(useJavaDoc = true)
    public RegistrationPage clickPrivacy()    {
        logger.info("Clicking privacy link");
        driverWait
                .until(ExpectedConditions.visibilityOf(privacyLink))
                .click();
        return this;
    }

    /**
     * Смена вкладки для ссылки на "Конфиденциальность". Данная ссылка открывается во второй вкладке
     * @return Страница с условиями конфиденциальности
     */
    @Step
    @Description(useJavaDoc = true)
    public PrivacyPage switchTabToPrivacyPage() {
        logger.info("Switching tab in browser to second tab with privacy");
        ArrayList tabs = new ArrayList (driver.getWindowHandles());
        logger.info("tabs id: "+tabs.toString());
        driver.switchTo().window((String) tabs.get(1));
        return new PrivacyPage(driver);
    }
    @Step
    @Description(useJavaDoc = true)
    public RegistrationPage changeTypeOfEmailField()   {
        logger.info("Changing type of email field");
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("document.getElementsByName('email')[0].setAttribute('type','text');");
        return this;
    }
}
