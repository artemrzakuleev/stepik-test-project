package page;

import io.qameta.allure.Description;
import io.qameta.allure.Flaky;
import io.qameta.allure.Step;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;

public class SideBarPage {
    private static final Logger logger= LoggerFactory.getLogger(SideBarPage.class);
    private final WebDriver driver;
    private final String pageUrl="https://stepik.org/lesson/12752/step/1?unit=3102";
    private final WebDriverWait driverWait;
    private final Duration duration=Duration.ofSeconds(15);
    /**
     * Конструктор с инициализацией полей класса
     */
    public SideBarPage(WebDriver driver) {
        PageFactory.initElements(driver,this);
        this.driver =driver;
        driverWait=new WebDriverWait(driver,duration);
    }
    @FindBy(xpath="//div[@class='lesson-sidebar__wrapper']")
    private WebElement sideBarWrapper;
    /**
     * Локатор боковой панели
     */
    @FindBy(xpath = "//div[@class='lesson-sidebar__body']")
    private WebElement sideBarBody;
    /**
     * Локатор кнопки открепления боковой панели
     */
    @FindBy(xpath ="//button[@aria-label='Unpin course contents sidebar']")
    private WebElement unpinSideBar;
    /**
     * Локатор кнопки прикрепления боковой панели
     */
    @FindBy(xpath = "//button[@aria-label='Pin course contents sidebar']")
    private WebElement pinSideBar;
    /**
     *Локатор элемента не относящегося к боковой панели ( необходимо для переноса фокуса с боковой панели)
     */
    @FindBy(xpath="//select[@class='st-select st-select_font_normal discussions__sorting']")
    private WebElement anotherElement;
    /**
     *Локатор кнопки раскрытия боковой панели
     */
    @FindBy(xpath="//button[@class='lesson-sidebar__expand-sidebar']")
    private WebElement expanderSideBar;
    /**
     * Метод для открепления боковой панели
     */
    @Step
    @Description(useJavaDoc = true)
    public SideBarPage unpinSideBar(){
        logger.info("Unpinning sidebar");
        new WebDriverWait(driver,duration).
                until(ExpectedConditions.elementToBeClickable(unpinSideBar)).
                click();
        return this;
    }
    /**
     * Метод для прикрепления боковой панели
     * Возможны проблемы в Firefox browser с перекрытием элементов связанные с анимацией разворачивания
     */
    @Flaky
    @Step
    @Description(useJavaDoc = true)
    public SideBarPage pinSideBar(){
        logger.info("Pinning sidebar");
        driverWait.
               until(ExpectedConditions.elementToBeClickable(pinSideBar));
        new Actions(driver)
                .moveToElement(pinSideBar)
                .build()
                .perform();
        pinSideBar.click();
        return this;
    }
    /**
     * Метод для переноса фокуса с боковой панели
     */
    @Step
    @Description(useJavaDoc = true)
    public SideBarPage changeFocus(){
        logger.info("Changing focus to other element and unfocusing sidebar");
        driverWait
                .until(ExpectedConditions.elementToBeClickable(anotherElement)).
                click();
        return this;
    }
    /**
     * Метод для проверки скрыта ли боковая панель
     */
    @Step
    @Description(useJavaDoc = true)
    public boolean isSideBarInvisible(){
        logger.info("Checking is sidebar invisible");
        driverWait
                .until(ExpectedConditions.invisibilityOf(sideBarBody));
        return !sideBarBody.isDisplayed();
    }
    /**
     * Метод для проверки развернута ли боковая панель
     */
    @Step
    @Description(useJavaDoc = true)
    public boolean isSideBarVisible(){
        logger.info("Checking is sidebar visible");
        return sideBarBody.isDisplayed();
    }
    /**
     * Метод для развертывания боковой панели
     */
    @Step
    @Description(useJavaDoc = true)
    public SideBarPage expandSideBar() {
        logger.info("Expanding sidebar");
        driverWait
                .until(ExpectedConditions.elementToBeClickable(expanderSideBar));
        Actions action=new Actions(driver);
        action.moveToElement(expanderSideBar).build().perform();
        return this;
    }
    /**
     * Метод ожидания скрытия боковой панели
     */
    @Step
    @Description(useJavaDoc = true)
    public SideBarPage waitUntilSideBarHide(){
        logger.info("Waiting until sidebar hide");
        driverWait
                .until(ExpectedConditions.visibilityOf(sideBarBody));
        return this;
    }
    /**
     * Открыть страницу
     */
    @Step
    @Description(useJavaDoc = true)
    public SideBarPage openPage()   {
        logger.info("Opening page {}",pageUrl);
        driver.get(pageUrl);
        driverWait
                .until(ExpectedConditions.visibilityOf(sideBarBody));
        return this;
    }
}
