package page;

import io.qameta.allure.Description;
import io.qameta.allure.Step;
import model.Comment;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.List;
import java.util.stream.Collectors;

public class DiscussionSortPage {
    private static final Logger logger= LoggerFactory.getLogger(DiscussionSortPage.class);
    private final WebDriver driver;
    private final Duration duration= Duration.ofSeconds(20);
    private final WebDriverWait driverWait;
    private final String pageURL="https://stepik.org/lesson/12752/step/1?unit=3102";
    /**
     * Конструктор с инициализацией полей класса
     */
    public DiscussionSortPage(WebDriver driver) {
        PageFactory.initElements(driver,this);
        this.driver=driver;
        driverWait=new WebDriverWait(driver,duration);
    }
    /**
     * Локатор select для выбора сортировки комментариев
     */
    @FindBy(xpath = "//select[@class='st-select st-select_font_normal discussions__sorting']")
    private WebElement selectSortingDiscussion;
    @FindBy(xpath = "//div[@class='comment-widget__body']")
    private WebElement discussionsList;
    @FindBy(xpath="//div[@class='discussions__list']")
    private WebElement discussionWidget;
    @FindBy(xpath = "//div[@class='comment-widget discussion_replies_hidden ember-view discussions__comment-widget']")
    private List<WebElement> commentListElements;
    @FindBy(xpath = "//button[@class='discussions__load-btn small white expand']")
    private WebElement expandDiscussionButton;

    /**
     * Показать обсуждения
     */
    @Description(useJavaDoc = true)
    public DiscussionSortPage expandDiscussion(){
        logger.info("Expanding discussion");
        new Actions(driver).
                moveToElement(discussionWidget)
                .click()
                .perform();
        return this;
    }
    /**
     *  Открыть страницу с сортировкой комментариев
     */
    @Description(useJavaDoc = true)
    public DiscussionSortPage openPage()  {
        logger.info("Opening page {}",pageURL);
        driver.get(pageURL);
        driverWait.
                until(ExpectedConditions.visibilityOf(selectSortingDiscussion));
        return this;
    }

    /**
     * Сменить сортировку комментариев
     */
    @Description(useJavaDoc = true)
    public DiscussionSortPage setSorting(String sorting)    {
        logger.info("Setting sorting by {}",sorting);
        Select selectElement=new Select(selectSortingDiscussion);
        selectElement.selectByValue(sorting);
        return this;
    }

    /**
     * Проверка сортировки "Новые обсуждения" (by date)
     */
    @Description(useJavaDoc = true)
    public boolean isSortingByDate()    {
        logger.info("Checking is sorting by date");
        driverWait.until(ExpectedConditions.visibilityOf(discussionsList));
        List<Comment> comments=commentListElements.stream().map(Comment::new).collect(Collectors.toList());
        List<Comment> sortingComments=comments.stream().sorted((o1, o2) -> o2.getDateTime().compareTo(o1.getDateTime())).collect(Collectors.toList());
        return comments.equals(sortingComments);
    }
    /**
     * Проверка сортировки "Самые популярные" (default)
     */
    @Description(useJavaDoc = true)
    public boolean isSortingByDefault()    {
        logger.info("Checking is sorting by default");
        driverWait.until(ExpectedConditions.visibilityOf(discussionsList));
        List<Comment> comments=commentListElements.stream().map(Comment::new).collect(Collectors.toList());
        List<Comment> sortingComments=comments.stream().sorted((o1, o2) -> o2.getRating() .compareTo(o1.getRating())).collect(Collectors.toList());
        return comments.equals(sortingComments);
    }
    /**
     * Проверка сортировки "Самые обсуждаемые" (active)
     */
    @Description(useJavaDoc = true)
    public boolean isSortingByActive()  {
        logger.info("Checking is sorting by active");
        driverWait.until(ExpectedConditions.visibilityOf(discussionsList));
        List<Comment> comments=commentListElements.stream().map(Comment::new).collect(Collectors.toList());
        List<Comment> sortingComments=comments.stream().sorted((o1, o2) -> o2.getReplies() .compareTo(o1.getReplies())).collect(Collectors.toList());
        return comments.equals(sortingComments);
    }

}
