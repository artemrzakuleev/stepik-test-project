package model;

public enum Language {
    RUSSIAN("Русский","Искать"),
    ENGLISH("English","Search"),
    GERMAN("Deutsch","Suche"),
    SPANISH("Español","Buscar");
    private final String textOfLanguageButton;
    private final String textOfSearchButton;
    Language(String textOfLanguageButton,String textOfSearchButton) {
        this.textOfLanguageButton=textOfLanguageButton;
        this.textOfSearchButton=textOfSearchButton;
    }
    public String getTextOfLanguageButton() {
        return textOfLanguageButton;
    }
    public String getTextOfSearchButton()   {
        return textOfSearchButton;
    }
}
