package model;

import lombok.Data;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import java.time.Instant;
import java.util.Comparator;

@Data
public class Comment {
    private Instant dateTime;
    private Integer thumbsUp;
    private Integer thumbsDown;
    private Integer rating;
    private Integer replies;
    private String author;
    private final String commentDateXpath=".//time[@class='comment-widget__date']";
    private final String  thumbsUpXpath=".//span[@class='vote-widget__segment vote-widget__segment_type_epic ']";
    private final String thumbsDownXpath=".//span[@class='vote-widget__segment vote-widget__segment_type_abuse ']";
    private final String repliesXpath=".//button[@class='comment-widget__show-replies comment-widget__footer-button btn-link link_no-static-line']";

    public Comment(WebElement element) {
        dateTime= Instant.parse( element.findElement(By.xpath(commentDateXpath)).getAttribute("datetime"));
        try {
            thumbsUp=Integer.parseInt(element.findElement(By.xpath(thumbsUpXpath)).getText());
        }
        catch (NumberFormatException e)
        {
            thumbsUp=0;
        }
        try {
            thumbsDown=Integer.parseInt(element.findElement(By.xpath(thumbsDownXpath)).getText());
        }
        catch (NumberFormatException e)
        {
            thumbsDown=0;
        }
        rating =thumbsUp-thumbsDown;
        try {
            String repliesText=element.findElement(By.xpath(repliesXpath)).getText();
            replies=Integer.parseInt(repliesText.substring(11,repliesText.indexOf("ответ")-1));
        }
        catch (NoSuchElementException e)    {
            replies=0;
        }
        catch (NumberFormatException e) {
            replies=0;
        }
    }
}
