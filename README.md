# Stepik Test Project

## Description
Test project combine UI and API automation tests for educational site [stepik.org](https://stepik.org).

## Contents
* [Language and Frameworks](#Language-and-Frameworks)
* [Installation](#Installation)   
  * [Using Git](#Using Git)   
  * [Using Jenkins](#Using Jenkins)
* [Execution of tests](#Execution of tests)
  * [Headless browsers](#Headless mode of browser)
  * [Browser](#Browser)
  * [Suite file](#Suite file)
  * [Parallel execution of tests](#parallel execution of tests)
* [Allure report](#Allure report)
* [Roadmap](#Roadmap)
* [Project Status](#Project status)

## Language and Frameworks
* Java as programming language
* Maven as build tool
* TestNG as test framework
* Selenium WebDriver as framework for UI tests
* WebDriverManager for managing browser drivers in Selenium
* RestAssured as framework for API tests
* Allure report tool as generator of test reports
* SLF4J + Log4J as logging framework

## Installation
### Using Git
Clone repository from Gitlab to local computer
``` bash
git clone https://gitlab.com/artemrzakuleev/stepik-test-project.git
cd stepik-test-project
```
### Using Jenkins
Create Jenkins Pipeline with "Pipeline Script from SCM".

## Execution of tests
Tests are running by Maven in commandline interface (Terminal, cmd.exe or Windows Terminal).
### Simple running of test suite
Run test. Default test suite is `UI.LoginTS`
```java
mvn test
```
### CLI commands for tests with options
```java
mvn test -D[option]=[parameter]
```
### Headless mode of browser 
Run UI tests in browser in headless or normal mode. Default mode is normal window with GUI.
- Running UI tests in headless browser.
```java
headless=true 
```
- Running UI tests in normal browser
```java
headless=false 
```
#### Example:
Running tests in headless mode of browser
```java
mvn test -Dheadless=true
```
### Browser
Choose browser for UI tests.  
- Run tests in Chrome browser
```java
browser=chrome
```
- Run tests in firefox browser
```java
browser=firefox
```
- Run tests in Opera browser
```java
browser=opera
```
- Run tests in firefox browser in docker container
```java
browser=firefox-in-docker
```
- Run tests in Chrome browser in docker container
```java
browser=chrome-in-docker
```  
#### Example
```java
mvn test -Dbrowser=firefox
```
### Suite file
Choose file with test suite. Test suites files are located in `src\test\resources\suite`.
Available variants are
- All tests
```java
suiteXmlFile=AllTestsTS
```
- API tests of login
```java
suiteXmlFile=ApiLoginTS
```
-  API tests of registration
```java
suiteXmlFile=ApiRegistrationTS
```
- UI tests of drop-down list
```java
suiteXmlFile=DropDownListTS
```
- UI tests of changing language of site
```java
suiteXmlFile=LanguageChangingTS
```
- UI tests of login
```java
suiteXmlFile=LoginTS
```
- UI tests of registration
```java
suiteXmlFile=RegistrationTS
```
- UI tests of sidebar
```java
suiteXmlFile=SideBarTS
```
#### Example
```java
mvn -DsuiteXmlFile=LoginTS test
```
### Parallel execution of tests
Tests can be run in parallel mode for reducing time of testing. Use option `threadCount` to select the number of parallel threads to test. Default value is `2`. Running tests in 8+ threads can increase time of testing on weak PC.
- Run tests in 1 thread.
```java
threadCount=1
```
- Run tests in 3 thread.
```java
threadCount=3
```
#### Example   
This command run default suite (LoginTS) with 3 windows of Chrome Browser. the fourth test will be performed after completing one of the first three.
```java
mvn  test -DthreadCount=3
```

### CLI parameters can be combined in various combinations.
#### Example: Run tests of login in 3 threads of headless browsers.
```java
mvn test -DsuiteXmlFile=LoginTS -Dheadless=true -Dbrowser=firefox -DthreadCount=3
```
## Allure report
`Stepik test project` includes `Allure report` to generate useful reports of test execution.   
#### Execute command "mvn allure:report" to generate folder "target\allure-results" with report of tests.
```java
mvn allure:report
```
#### Then, execute command "mvn allure:serve" to serve report and run site in web browser with report.
```java
mvn allure:serve
```
### Example of Allure report
![Allure report]( https://gitlab.com/artemrzakuleev/stepik-test-project/-/raw/attachments_readme.md/.attachments/allure%20report.jpg )

## Roadmap
### Common functions
- [x] commands for environment configuration
- [x] Test Suites
- [x] Allure report
- [ ] Jenkinsfile
### UI Selenium
- [x] Login
- [x] Registration
- [x] Sidebar
- [x] Drop-down list
- [x] Changing language
- [x] Search
- [x] Sort of comments
- [x] configuration of headless browsers
- [x] Browsers in Docker containers
- [x] MultiThreading UI tests
### Rest API
- [x] Login
- [x] Registration

## Project status
In progress.
